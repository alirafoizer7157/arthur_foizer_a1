# Welcome to TreasureHunt!!

## Installation

To run my game, all you need to do is:

1. Pull the game files onto your computer.
2. Open the Solution (.sln) file using Visual Studio
3. Run the solution and enjoy the game!! :)

P.S. All game related information like instructions and about is already included in the game.

## License
[MIT](https://choosealicense.com/licenses/mit/)

My choice of using the MIT License is due to the fact that a permissive license is a better fit for this type of project I have on my repository.
Not only is this license short and straightforward, but it is also permissive of content modification and improvements without sacrificing the preservation of copyright and license notices.