﻿/* GameScene.cs
 * 
 * Arthur Foizer's Final Project - Treasure Hunt Game.
 * 
 * Revision History 
 *      Arthur Foizer, 2021.12.02: Created 
 *      Arthur Foizer, 2021.12.02: Added code
 *      Arthur Foizer, 2021.12.10: Debugging complete 
 *      Arthur Foizer, 2021.12.10: Comments added
 *       
 */
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreasureHunt
{
    /// <summary>
    /// GameScene class, responsible for holding the Game logic on the DrawableGameComponent context.
    /// </summary>
    class GameScene : DrawableGameComponent
    {
        public List<GameComponent> Components;

        /// <summary>
        /// Method responsible for the showing logic.
        /// </summary>
        public virtual void Show()
        {
            this.Enabled = true;
            this.Visible = true;
        }

        /// <summary>
        /// Method responsible for the hidding logic.
        /// </summary>
        public virtual void Hide()
        {
            this.Enabled = false;
            this.Visible = false;
        }

        /// <summary>
        /// GameScene default constructor.
        /// </summary>
        /// <param name="game"></param>
        public GameScene(Game game)
            : base(game)
        {
            Components = new List<GameComponent>();
            Hide();
        }

        /// <summary>
        /// Update logic for the GameScene component.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            // call the update logic of each item.
            foreach (GameComponent item in Components)
            {
                item.Update(gameTime);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Drawing logic for the GameScene component.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            DrawableGameComponent component = null;

            // call the drawing logic of each item.
            foreach (DrawableGameComponent item in Components)
            {
                component = (DrawableGameComponent)item;

                // show it.
                if (component.Visible)
                {
                    component.Draw(gameTime);
                }
            }

            base.Draw(gameTime);
        }
    }
}
