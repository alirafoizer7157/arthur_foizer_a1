﻿/* MenuComponent.cs
 * 
 * Arthur Foizer's Final Project - Treasure Hunt Game.
 * 
 * Revision History 
 *      Arthur Foizer, 2021.12.02: Created 
 *      Arthur Foizer, 2021.12.02: Added code
 *      Arthur Foizer, 2021.12.10: Debugging complete 
 *      Arthur Foizer, 2021.12.10: Comments added
 *       
 */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreasureHunt
{
    /// <summary>
    /// MenuComponent class, responsible for holding the Menu screen logic on the DrawableGameComponent context.
    /// </summary>
    class MenuComponent : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private Vector2 position;
        private List<Texture2D> menuButtons;
        private KeyboardState currKeyboardState, prevKeyboardState;

        public int SelectedIndex;

        /// <summary>
        /// MenuComponent default constructor.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="spriteBatch"></param>
        /// <param name="menuButtons"></param>
        public MenuComponent(Game game, SpriteBatch spriteBatch, List<Texture2D> menuButtons)
            :base (game)
        {
            this.spriteBatch = spriteBatch;
            this.position = new Vector2(Shared.ScreenDimension.X / 6, Shared.ScreenDimension.Y / 2);
            this.menuButtons = menuButtons;
        }

        /// <summary>
        /// Update logic for the Menu component.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            currKeyboardState = Keyboard.GetState();

            // if the Right key is pressed, we go to the next button index, which will be to the right.
            if (currKeyboardState.IsKeyDown(Keys.Right) && prevKeyboardState.IsKeyUp(Keys.Right))
            {
                SelectedIndex++;

                if (SelectedIndex == menuButtons.Count)
                {
                    SelectedIndex = 0;
                }
            }

            // if the Left key is pressed, we go to the previous button index, which will be to the left.
            if (currKeyboardState.IsKeyDown(Keys.Left) && prevKeyboardState.IsKeyUp(Keys.Left))
            {
                SelectedIndex--;

                if (SelectedIndex < 0)
                {
                    SelectedIndex = menuButtons.Count - 1;
                }
            }

            prevKeyboardState = currKeyboardState;

            base.Update(gameTime);
        }

        /// <summary>
        /// Drawing logic for the Menu component.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            Vector2 itemPosition = position;

            spriteBatch.Begin();

            // drawing each button - Play, About, Help, Exit.
            for (int n = 0; n < menuButtons.Count; n++)
            {
                if (SelectedIndex == n)
                {
                    spriteBatch.Draw(menuButtons[n], itemPosition, Color.Red);
                    itemPosition.X += 180;
                }

                else
                {
                    spriteBatch.Draw(menuButtons[n], itemPosition, Color.White);
                    itemPosition.X += 180;
                }
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
