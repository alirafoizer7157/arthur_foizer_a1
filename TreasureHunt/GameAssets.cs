﻿/* GameAssets.cs
 * 
 * Arthur Foizer's Final Project - Treasure Hunt Game.
 * 
 * Revision History 
 *      Arthur Foizer, 2021.11.29: Created 
 *      Arthur Foizer, 2021.11.29: Added code
 *      Arthur Foizer, 2021.11.30: Added code
 *      Arthur Foizer, 2021.12.01: Added code
 *      Arthur Foizer, 2021.12.10: Debugging complete 
 *      Arthur Foizer, 2021.12.10: Comments added
 *       
 */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Text;
using static TreasureHunt.GameWorld;

namespace TreasureHunt
{
    /// <summary>
    /// GameAssets class, responsible for the games logic and structure
    /// </summary>
    public class GameAssets
    {
        // creating a Struct to hold a cell's attributes.
        private struct Cell
        {
            public bool hasBomb;
            public bool exploded;
            public bool hasTreasure;
            public bool discovered;
            public int bombsNearby;
            public Rectangle position;
            public Texture2D image;
        }

        // creating an Enum to hold the Game States.
        public enum GameStates
        {
            PlayerWin,
            PlayerLose,
            GameNotOver,
            MainMenu
        }

        // declaring an instace of the Explosion class.
        public Explosion explosion;

        private Cell[,] cell = new Cell[12, 12]; // although the game board is 10x10, we need a cell border region (Row 0 & Column 0 || Row 12 & Column 12).
        private MouseState currMouseState, prevMouseState;
        private Texture2D gameBackground, bombImg, treasureImg, blankImg;
        private Texture2D[] numbers = new Texture2D[9];
        private Song[] musics = new Song[3];
        private SoundEffect effect;
        private Random random;
        private int cell_size = 50;

        public GameStates GameState;
        public SpriteFont Display, Dialogue;
        public Vector2 DisplayPosition = new Vector2(250, 610);
        public int Score = 0, Multiplier = 100;

        /// <summary>
        /// Method responsible for loading the game's contents.
        /// </summary>
        /// <param name="Content"></param>
        public void Load(ContentManager Content)
        {
            // loading the background image.
            gameBackground = Content.Load<Texture2D>("Images/map");

            // loading the display (Score and Number of Turns left) font.
            Display = Content.Load<SpriteFont>("Fonts/display");
            Dialogue = Content.Load<SpriteFont>("Fonts/dialogue");

            // loading the new game board with blank cells.
            for (int row = 1; row < 10; row++)
            {
                for (int col = 1; col < 10; col++)
                {
                    cell[row, col].image = Content.Load<Texture2D>("Images/blank");
                }
            }

            // loading the main elements images
            bombImg = Content.Load<Texture2D>("Images/bomb");
            treasureImg = Content.Load<Texture2D>("Images/treasure");
            blankImg = Content.Load<Texture2D>("Images/blank");

            explosion = new Explosion(Content);

            // loading the numbers images
            for (int i = 0; i < numbers.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        numbers[i] = Content.Load<Texture2D>("Images/zero");
                        break;

                    case 1:
                        numbers[i] = Content.Load<Texture2D>("Images/one");
                        break;

                    case 2:
                        numbers[i] = Content.Load<Texture2D>("Images/two");
                        break;

                    case 3:
                        numbers[i] = Content.Load<Texture2D>("Images/three");
                        break;

                    case 4:
                        numbers[i] = Content.Load<Texture2D>("Images/four");
                        break;

                    case 5:
                        numbers[i] = Content.Load<Texture2D>("Images/five");
                        break;

                    case 6:
                        numbers[i] = Content.Load<Texture2D>("Images/six");
                        break;

                    case 7:
                        numbers[i] = Content.Load<Texture2D>("Images/seven");
                        break;

                    case 8:
                        numbers[i] = Content.Load<Texture2D>("Images/eight");
                        break;
                }
            }

            // loading the game music and sound effects.
            musics[0] = Content.Load<Song>("Audio/pirate-music");
            musics[1] = Content.Load<Song>("Audio/boom");
            musics[2] = Content.Load<Song>("Audio/victory");

            effect = Content.Load<SoundEffect>("Audio/dig");

            MediaPlayer.Play(musics[0]);
            MediaPlayer.IsRepeating = true;
        }

        /// <summary>
        /// Method responsible for the game loop.
        /// </summary>
        public void Update(GameTime gameTime)
        {
            currMouseState = Mouse.GetState();

            int row, column;

            column = (currMouseState.X / cell_size) - 4;
            row = (currMouseState.Y / cell_size) - 1;

            // if the player clicks outside of the gameboard.
            if (row > 10 || column > 10)
            {
                return;
            }

            // if the game hasn't ended...
            if (GameState == GameStates.GameNotOver)
            {
                // if a confirmed click was made by the player...
                if (currMouseState.LeftButton == ButtonState.Pressed && prevMouseState.LeftButton == ButtonState.Released)
                {
                    effect.Play();

                    // and that cell has not been selected yet...
                    if (!cell[row, column].discovered)
                    {
                        cell[row, column].discovered = true;

                        if (cell[row, column].hasBomb)
                        {
                            // the game is over, they've found a bomb and the explosion animation starts.
                            GameState = GameStates.PlayerLose;

                            Score = 0;

                            cell[row, column].exploded = true;

                            explosion.Position = new Vector2(cell[row, column].position.X, cell[row, column].position.Y);
                            explosion.Enable = true;

                            // Explosion sound effect
                            MediaPlayer.Play(musics[1]);
                            MediaPlayer.IsRepeating = false;
                        }

                        else if (cell[row, column].hasTreasure)
                        {
                            // the game is over, they've found the treasure!
                            GameState = GameStates.PlayerWin;

                            // Final Score
                            Score = 1000 * Multiplier;

                            // Victory music.
                            MediaPlayer.Play(musics[2]);
                            MediaPlayer.IsRepeating = false;
                        }

                        // the multiplier will decrease with the amount of clicks the player does.
                        else
                        {
                            Multiplier--;
                        }
                    }
                }
            }

            explosion.Update(gameTime);

            prevMouseState = currMouseState;
        }

        /// <summary>
        /// Method responsible for drawing our games contents at each loop.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(gameBackground, Vector2.Zero, null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0);

            for (int row = 1; row <= 10; row++)
            {
                for (int col = 1; col <= 10; col++)
                {
                    // if the cell hasn't been selected yet.
                    if (!cell[row, col].discovered)
                    {
                        spriteBatch.Draw(blankImg, cell[row, col].position, Color.White);
                    }

                    // if the cell has been discovered (selected).
                    else if (cell[row, col].discovered)
                    {
                        // if it is a bomb, show the bomb and the explosion animation.
                        if (cell[row, col].hasBomb)
                        {
                            spriteBatch.Draw(bombImg, cell[row, col].position, Color.White);
                            explosion.Draw(gameTime, spriteBatch);
                        }

                        // if it is the treasure, show the treasure and the player's final score.
                        else if (cell[row, col].hasTreasure)
                        {
                            spriteBatch.Draw(treasureImg, cell[row, col].position, Color.White);
                            spriteBatch.DrawString(Display, "Final Score: " + Score.ToString(), new Vector2(250, 635), Color.White);
                        }

                        // if it is not a bomb nor the treasure, it is a number, also showing the amount of turns left.
                        else
                        {
                            spriteBatch.Draw(numbers[cell[row, col].bombsNearby], cell[row, col].position, Color.White);
                            spriteBatch.DrawString(Display, "Chances Left: " + Multiplier.ToString(), DisplayPosition, Color.White);
                        }
                    }
                }
            }

            // Now we will check all the appropriate game states (PlayerLose x PlayerWin).
            // if the Player loses (a bomb has been found), we reveal all the bombs and treasure locations, and display failure message.. 
            if (GameState == GameStates.PlayerLose)
            {
                spriteBatch.DrawString(Dialogue, "You Lose :( Better luck next time...\nPress ESC to go back to the Menu", new Vector2(250, 25), Color.Red);

                for (int row = 0; row <= 10; row++)
                {
                    for (int col = 0; col <= 10; col++)
                    {
                        if (cell[row, col].hasBomb && !cell[row, col].exploded)
                        {
                            spriteBatch.Draw(bombImg, cell[row, col].position, Color.White);
                        }

                        else if (cell[row, col].hasTreasure)
                        {
                            spriteBatch.Draw(treasureImg, cell[row, col].position, Color.White);
                        }
                    }
                }
            }

            // If the player finds a treasure, game ends, and success message is displayed.
            else if (GameState == GameStates.PlayerWin)
            {
                spriteBatch.DrawString(Dialogue, "Congratulations!! You Won!! :)\nPress ESC to go back to the Menu", new Vector2(250, 25), Color.Red);
            }
        }

        /// <summary>
        /// Method responsible for creating our gameboard,
        /// setting each cell based on the parameters specified by our Cell struct.
        /// </summary>
        public void BuildBoard()
        {
            // setting up the initial state of our gameboard.
            for (int row = 0; row < 12; row++)
            {
                for (int col = 0; col < 12; col++)
                {
                    cell[row, col].hasBomb = false;
                    cell[row, col].exploded = false;
                    cell[row, col].hasTreasure = false;
                    cell[row, col].discovered = false;
                    cell[row, col].bombsNearby = 0;

                    cell[row, col].position.Width = cell_size;
                    cell[row, col].position.Height = cell_size;
                    cell[row, col].position.X = (col + 4) * cell_size;
                    cell[row, col].position.Y = (row + 1) * cell_size;
                }
            }
        }

        /// <summary>
        /// Method responsible for stting up the positions of our game elements.
        /// </summary>
        public void SetPositions()
        {
            random = new Random();

            bool[] bomb = new bool[100];

            int treasureId = random.Next(90);

            // setting up the correct amount of bombs.
            for (int i = 0; i < 100; i++)
            {
                // 90 cells don't have bombs (futurelly one of them will have the treasure).
                if (i < 90)
                {
                    bomb[i] = false;
                }

                // 10 cells have bombs
                else
                {
                    bomb[i] = true;
                }
            }

            // we are scrambling the bomb positions here (without the risk of losing the Treasure).
            for (int i = 0; i < 100; i++)
            {
                int scramble = random.Next(100);
                bool savedValue = bomb[i];

                if (i != treasureId)
                {
                    bomb[i] = bomb[scramble];
                    bomb[scramble] = savedValue;
                }
            }

            // we are setting our cells attributes based on our array values (if it has a bomb, or a treasure, or nothing (number)).
            for (int i = 0; i < 100; i++)
            {
                // this logic makes sure only our playable cells are set to be played (i.e. rows and columns 1 to 11, and not 0 to 12 (our blank rows and columns)).
                int rowNumber = (i / 10) + 1, columnNumber = (i % 10) + 1;

                // we check which random number was picked to be our treasure, and we mark that cell as having the treasure
                if (i == treasureId)
                {
                    cell[rowNumber, columnNumber].hasTreasure = true;
                }
                else
                {
                    cell[rowNumber, columnNumber].hasBomb = bomb[i];
                }
            }
        }

        /// <summary>
        /// Method responsible for counting the number of bombs surrounded by our selected cell,
        /// which will determine which number will appear on each cell, that's not a bomb or the treasure.
        /// </summary>
        public void CountingBombs()
        {
            for (int row = 1; row <= 10; row++)
            {
                for (int col = 1; col <= 10; col++)
                {
                    // we are now checking for how many bombs are in our neighborhood.
                    // on the widest case, we need to check 8 scenarios.
                    // i.e. if we are on cell[1,1] ([row,col]);
                    //      we'll look for bombs on the cells: [0,0] || [0,1] || [0,2]
                    //                                         -----------------------
                    //                                         -----------------------
                    //                                         [1,0] ||       || [1,2]
                    //                                         -----------------------
                    //                                         -----------------------
                    //                                         [2,0] || [2,1] || [2,2]

                    int bombCount = 0;

                    // we are only counting cells that are not bombs.
                    if (!cell[row, col].hasBomb)
                    {
                        // Upper row.
                        if (cell[row - 1, col - 1].hasBomb)
                        {
                            bombCount++;
                        }

                        if (cell[row - 1, col].hasBomb)
                        {
                            bombCount++;
                        }

                        if (cell[row - 1, col + 1].hasBomb)
                        {
                            bombCount++;
                        }

                        // Same row.
                        if (cell[row, col - 1].hasBomb)
                        {
                            bombCount++;
                        }

                        if (cell[row, col + 1].hasBomb)
                        {
                            bombCount++;
                        }

                        // Lower row.
                        if (cell[row + 1, col - 1].hasBomb)
                        {
                            bombCount++;
                        }

                        if (cell[row + 1, col].hasBomb)
                        {
                            bombCount++;
                        }

                        if (cell[row + 1, col + 1].hasBomb)
                        {
                            bombCount++;
                        }

                        // total number of bombs on the neighbourhood (0 to 8).
                        cell[row, col].bombsNearby = bombCount;
                    }
                }
            }
        }
    }
}
