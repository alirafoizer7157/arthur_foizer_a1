﻿/* StartScene.cs
 * 
 * Arthur Foizer's Final Project - Treasure Hunt Game.
 * 
 * Revision History 
 *      Arthur Foizer, 2021.12.02: Created 
 *      Arthur Foizer, 2021.12.02: Added code
 *      Arthur Foizer, 2021.12.10: Debugging complete 
 *      Arthur Foizer, 2021.12.10: Comments added
 *       
 */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreasureHunt
{
    /// <summary>
    /// StartScene class, responsible for holding the Menu screen logic on the GameScene context.
    /// </summary>
    class StartScene : GameScene
    {
        public MenuComponent Menu;

        private Texture2D background;
        private SpriteBatch spriteBatch;
        List<Texture2D> menuButtons = new List<Texture2D>();

        /// <summary>
        /// StartScene default constructor, loading the menu content.
        /// </summary>
        /// <param name="game"></param>
        public StartScene(Game game)
            : base(game)
        {
            GameWorld gameWorld = ((GameWorld)game);
            this.spriteBatch = gameWorld.spriteBatch;
            background = gameWorld.Content.Load<Texture2D>("Images/menubg");
            menuButtons.Add(gameWorld.Content.Load<Texture2D>("Buttons/playbutton"));
            menuButtons.Add(gameWorld.Content.Load<Texture2D>("Buttons/aboutbutton"));
            menuButtons.Add(gameWorld.Content.Load<Texture2D>("Buttons/helpbutton"));
            menuButtons.Add(gameWorld.Content.Load<Texture2D>("Buttons/exitbutton"));

            Menu = new MenuComponent(game, spriteBatch, menuButtons);

            this.Components.Add(Menu);
        }

        /// <summary>
        /// Update logic for the StartScene class.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Drawing logic for the StartScene class.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
