﻿/* GameWorld.cs
 * 
 * Arthur Foizer's Final Project - Treasure Hunt Game.
 * 
 * Revision History 
 *      Arthur Foizer, 2021.11.29: Created 
 *      Arthur Foizer, 2021.11.29: Added code
 *      Arthur Foizer, 2021.12.01: Added code
 *      
 *      Arthur Foizer, 2020.01.03: Debugging complete 
 *      Arthur Foizer, 2020.01.02: Comments added
 *      
 */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using static TreasureHunt.GameAssets;

namespace TreasureHunt
{
    public class GameWorld : Game
    {
        private GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;

        private StartScene startScene;
        private AboutScene aboutScene;
        private HelpScene helpScene;
        private ActionScene actionScene;

        private int ScreenWidth = 1025;
        private int ScreenHeight = 720;

        public GameWorld()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = ScreenWidth;
            graphics.PreferredBackBufferHeight = ScreenHeight;
            graphics.ApplyChanges();

            Shared.ScreenDimension = new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            IsMouseVisible = true;

            base.Initialize();
        }

        private void HideAllScenes()
        {
            foreach (GameComponent component in Components)
            {
                if (component is GameScene)
                {
                    ((GameScene)component).Hide();
                }
            }
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            startScene = new StartScene(this);
            this.Components.Add(startScene);
            startScene.Show();

            aboutScene = new AboutScene(this);
            this.Components.Add(aboutScene);

            helpScene = new HelpScene(this);
            this.Components.Add(helpScene);

            actionScene = new ActionScene(this);
            this.Components.Add(actionScene);
        }

        // Game Looping.
        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            int selectedIndex = 0;

            if (startScene.Enabled)
            {
                selectedIndex = startScene.Menu.SelectedIndex;

                if (selectedIndex == 0 && keyboardState.IsKeyDown(Keys.Enter))
                {
                    HideAllScenes();
                    actionScene = new ActionScene(this);
                    this.Components.Add(actionScene);
                    actionScene.Show();
                }

                else if (selectedIndex == 1 && keyboardState.IsKeyDown(Keys.Enter))
                {
                    HideAllScenes();
                    aboutScene.Show();
                }

                else if (selectedIndex == 2 && keyboardState.IsKeyDown(Keys.Enter))
                {
                    HideAllScenes();
                    helpScene.Show();
                }

                else if (selectedIndex == 3 && keyboardState.IsKeyDown(Keys.Enter))
                {
                    Exit();
                }
            }

            bool exitScene = actionScene.Enabled || aboutScene.Enabled || helpScene.Enabled;

            if (exitScene)
            {
                if (keyboardState.IsKeyDown(Keys.Escape))
                {
                    HideAllScenes();
                    startScene.Show();
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Beige);

            spriteBatch.Begin();

            //gameAssets.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
