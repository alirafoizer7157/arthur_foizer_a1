﻿/* Explosion.cs
 * 
 * Arthur Foizer's Final Project - Treasure Hunt Game.
 * 
 * Revision History 
 *      Arthur Foizer, 2021.12.05: Created 
 *      Arthur Foizer, 2021.12.05: Added code
 *      Arthur Foizer, 2021.12.10: Debugging complete 
 *      Arthur Foizer, 2021.12.10: Comments added
 *       
 */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreasureHunt
{
    /// <summary>
    /// Explosion class, responsible for the Explosion animation logic
    /// </summary>
    public class Explosion
    {
        private Texture2D animation;
        private Vector2 dimension;
        private List<Rectangle> frames;
        private int frameIndex = -1;
        private int delay = 5;
        private int delayCounter = 0;

        public Vector2 Position;
        public bool Enable;

        /// <summary>
        /// Explosion default constructor.
        /// </summary>
        /// <param name="Content"></param>
        public Explosion(ContentManager Content)
        {
            animation = Content.Load<Texture2D>("Images/explosion");
            dimension = new Vector2(64, 64);
            Position = Vector2.Zero;

            GenerateFrames();

            Enable = false;
        }

        /// <summary>
        /// Method responsible for building the frames array for our animation.
        /// </summary>
        private void GenerateFrames()
        {
            frames = new List<Rectangle>();

            // going through the image and setting up a new rectangle for each frame.
            for (int row = 0; row < 5; row++)
            {
                for (int col = 0; col < 5; col++)
                {
                    int x = col * (int)dimension.X;
                    int y = row * (int)dimension.Y;

                    Rectangle grid = new Rectangle(x, y, (int)dimension.X, (int)dimension.Y);

                    frames.Add(grid);
                }
            }
        }

        /// <summary>
        /// Method responsible for the loop logic for the explosion.
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            // if the explosion was activated...
            if (Enable)
            {
                delayCounter++;

                // going through each frame from our image.
                if (delayCounter > delay)
                {
                    frameIndex++;

                    if (frameIndex > 24)
                    {
                        frameIndex = -1;
                        Enable = false;
                    }

                    delayCounter = 0;
                }
            }
        }

        /// <summary>
        /// Method responsible for drawing the explosion.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="spriteBatch"></param>
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            // if it is enabled and the frame index is not the first one, draw.
            if((Enable) && (frameIndex > 0))
                spriteBatch.Draw(animation, Position, frames[frameIndex], Color.White);
        }
    }
}
