﻿/* ActionScene.cs
 * 
 * Arthur Foizer's Final Project - Treasure Hunt Game.
 * 
 * Revision History 
 *      Arthur Foizer, 2021.12.02: Created 
 *      Arthur Foizer, 2021.12.02: Added code
 *      Arthur Foizer, 2021.12.10: Debugging complete 
 *      Arthur Foizer, 2021.12.10: Comments added
 *       
 */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using static TreasureHunt.GameAssets;

namespace TreasureHunt
{
    /// <summary>
    /// ActionScene class, responsible for holding our actual game on the GameScene context.
    /// </summary>
    class ActionScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private GameAssets gameAssets;

        /// <summary>
        /// Default constructor for the ActionScene class.
        /// </summary>
        /// <param name="game"></param>
        public ActionScene(Game game)
            : base(game)
        {
            // loading our game content and spritebatch.
            this.spriteBatch = ((GameWorld)game).spriteBatch;
            ContentManager Content = ((GameWorld)game).Content;

            // loading the actual game logic behind it.
            gameAssets = new GameAssets();

            // default gamestate.
            gameAssets.GameState = GameStates.GameNotOver;

            // calling the GameAssets methods that build the game structure.
            gameAssets.BuildBoard();
            gameAssets.SetPositions();
            gameAssets.CountingBombs();

            // GameAssets Load() method being called.
            gameAssets.Load(Content);
        }

        /// <summary>
        /// Method responsible for our game loop.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            // GameAssets Update() method being called.
            gameAssets.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// Method responsible for drawing our game.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            // GameAssets Draw() method being called.
            gameAssets.Draw(gameTime, spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
