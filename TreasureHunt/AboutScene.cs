﻿/* AboutScene.cs
 * 
 * Arthur Foizer's Final Project - Treasure Hunt Game.
 * 
 * Revision History 
 *      Arthur Foizer, 2021.12.02: Created 
 *      Arthur Foizer, 2021.12.02: Added code
 *      Arthur Foizer, 2021.12.10: Debugging complete 
 *      Arthur Foizer, 2021.12.10: Comments added
 *       
 */
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreasureHunt
{
    /// <summary>
    /// AboutScene class, responsible for holding the About screen logic on the GameScene context.
    /// </summary>
    class AboutScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private Texture2D aboutScreen;

        /// <summary>
        /// AboutScene default contructor.
        /// </summary>
        /// <param name="game"></param>
        public AboutScene(Game game)
            : base(game)
        {
            this.spriteBatch = ((GameWorld)game).spriteBatch;
            aboutScreen = ((GameWorld)game).Content.Load<Texture2D>("Images/aboutbg");
        }

        /// <summary>
        /// Update logic for the about page.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Drawing logic for the about page.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            // draw the AboutScreen image on the application.
            spriteBatch.Draw(aboutScreen, Vector2.Zero, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
